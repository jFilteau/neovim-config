" Plugins
call plug#begin('~/.local/share/nvim/plugged')
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'rakr/vim-one'
Plug 'joshdick/onedark.vim'
Plug 'itchyny/lightline.vim'
Plug 'Raimondi/delimitMate'
Plug 'airblade/vim-gitgutter'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'w0rp/ale'
Plug 'scrooloose/nerdcommenter'
Plug 'jalvesaq/Nvim-R/', {'tag': 'v0.9.12'} " We're using this tage because the latest version removed suport for \pa
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }
call plug#end()

let g:python3_host_prog = '/usr/bin/python3.6'
let g:netrw_dirhistmax = 0

" Removes unwanted characters when pressing keys
set guicursor=

" General settings
syntax on
filetype plugin indent on
set tabstop=4
set shiftwidth=4
set softtabstop=4 
set expandtab
set number relativenumber
set incsearch
set cursorline
set showmatch
set noswapfile
set backupdir=~/.config/nvim/tmp/backup

" Themes
:set background=dark " for the dark version
let g:lightline = {'colorscheme': 'one'}
colorscheme onedark

" NERDtree
nmap <F6> :NERDTreeToggle<CR>
autocmd VimEnter * NERDTree " automatically open on startup
let NERDTreeWinSize = 27

" Autocompletion
let g:deoplete#enable_at_startup = 1

" Linters
let g:ale_linters = {}
let g:ale_linters['javascript'] = ['eslint']
let g:ale_linters['python3'] = ['flake8']

" Keyboard mappings
nmap <C-S> :update<CR>
nmap <S-Up> :m-2<CR>
nmap <S-Down> :m+<CR>
nmap ,c :call NERDComment(0, "toggle")<CR>

" Nvim-R config
let R_assign = 0
let R_clear_line = 1
let R_rconsole_width = 65 " Add these two if you want a vertical split
let R_min_editor_width = 18
let R_complete = 2
nmap <Space> <Plug>RDSendSelection
nmap <Space> <Plug>RDSendLine
imap %% %>%
"let R_rconsole_height = 5 " Add this if you want a horizontal split
